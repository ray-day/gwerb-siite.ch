module.exports = {
  siteTitle: 'Gwerbsiite', // <title>
  manifestName: 'gwerbsiite',
  manifestShortName: 'Landing', // max 12 characters
  manifestStartUrl: '/',
  manifestBackgroundColor: '#663399',
  manifestThemeColor: '#663399',
  manifestDisplay: 'standalone',
  manifestIcon: 'src/assets/img/zuriflag.jpeg',
  pathPrefix: `/gatsby-starter-overflow/`, // This path is subpath of your hosting https://domain/portfolio
  authorName: 'Wir bauen deine Webseite',
  heading: 'Corona Hilf us Züri für Züri',
  // social
  socialLinks: [
    {
      icon: 'fa-phone',
      name: 'Telefon',
      url: 'tel:0792016803',
    },
    // {
    //   icon: 'fa-skype',
    //   name: 'Telefon',
    //   url: 'tel:raymanza',
    // },
    // {
    //   icon: 'fa-twitter',
    //   name: 'Twitter',
    //   url: 'https://twitter.com/onlyanubhav',
    // },
    // {
    //   icon: 'fa-facebook',
    //   name: 'Facebook',
    //   url: 'https://facebook.com/theanubhav',
    // },
    // {
    //   icon: 'fa-envelope-o',
    //   name: 'Email',
    //   url: 'mailto:info@',
    // },
  ],
};
