import React from 'react';
import Scroll from './Scroll';
import config from '../../config';

export default function Header({ title, heading, avatar }) {
  return (
    <section id="header">
      <header>
        <h2>{config.authorName}</h2>
        <p>{config.heading}</p>
        <br></br>
      </header>
      <footer>
        <Scroll type="id" element="banner">
          <a href="#banner" className="button style2 ">
            Gratis?
          </a>
        </Scroll>
      </footer>
    </section>
  );
}
