import React from 'react';

import Layout from '../components/Layout';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { Link } from 'gatsby';
import pic1 from '../assets/images/www.jpeg';
import pic2 from '../assets/images/pic02.jpg';
import pic3 from '../assets/images/startup.jpg';
import pic4 from '../assets/images/tech.jpg';

import Scroll from '../components/Scroll';
// import Gallery from '../components/Gallery';

// const ROW1_IMAGES = [
//   {
//     src: require('../assets/images/fulls/01.jpg'),
//     thumbnail: require('../assets/images/thumbs/01.jpg'),
//     caption: 'Ad infinitum',
//   },
//   {
//     src: require('../assets/images/fulls/02.jpg'),
//     thumbnail: require('../assets/images/thumbs/02.jpg'),
//     caption: 'Dressed in Clarity',
//   },
//   {
//     src: require('../assets/images/fulls/03.jpg'),
//     thumbnail: require('../assets/images/thumbs/03.jpg'),
//     caption: 'Raven',
//   },
//   {
//     src: require('../assets/images/fulls/04.jpg'),
//     thumbnail: require('../assets/images/thumbs/04.jpg'),
//     caption: "I'll have a cup of Disneyland, please",
//   },
// ];

// const ROW2_IMAGES = [
//   {
//     src: require('../assets/images/fulls/05.jpg'),
//     thumbnail: require('../assets/images/thumbs/05.jpg'),
//     caption: 'Cherish',
//   },
//   {
//     src: require('../assets/images/fulls/06.jpg'),
//     thumbnail: require('../assets/images/thumbs/06.jpg'),
//     caption: 'Different.',
//   },
//   {
//     src: require('../assets/images/fulls/07.jpg'),
//     thumbnail: require('../assets/images/thumbs/07.jpg'),
//     caption: 'History was made here',
//   },
//   {
//     src: require('../assets/images/fulls/08.jpg'),
//     thumbnail: require('../assets/images/thumbs/08.jpg'),
//     caption: 'People come and go and walk away',
//   },
// ];
const IndexPage = () => (
  <Layout>
    <Header />

    <section id="banner">
      <header>
        <h2>Gratis leider nicht </h2>
        <br></br>
        <h3>aber vielleicht fast</h3>
      </header>
      <p>

          Wir arbeiten ein Paar Stunden lang gratis - unser Corona Angebot <br></br> 
        
        
          {/* Wenn Sie alles Material haben, dann bringen wir die Webseite gratis */}
          {/* Je nach Fall können wir mit der Benutzung unserer Vorlagen innerhalb von 3 Stunden eine Webseite oder einen Entwurf erstellen. */}
          <br></br>
          {/* Am einfachsten ist es wenn Sie bereits eine Webseite haben und wir den Text und die Bilder von dieser nehmen können */}
          <br></br>
          Wir benutzen Templates - das spart Zeit und Geld.
          <br></br>
          Erklär uns was du brauchst und wir melden uns bei dir.
          <br></br>
        
          

        {/* A brand new site template designed by{' '}
        <a href="http://twitter.com/ajlkn">AJ</a> for{' '}
        <a href="http://html5up.net">HTML5 UP</a>.<br /> */}
{/* 
        Wir designen wirklich gerne Webseiten.
        <br />
        {' '}
       Es gibt zu viele Firmen die eine alte Webseite haben und noch viel mehr,<br></br> welche nicht von ihrer Webseite profitieren.
      
        <a href="http://html5up.net/license">Creative Commons license</a>.
        <br></br>
        Corona Hilfe für Zürich ist für uns eine Initiative um potentielle Kunden zu finden.
        <br></br>
         */}
      </p>
      <footer>
        <Scroll type="id" element="first">
          <a href="#first" className="button style2 scrolly">
           Mehr dazu
          </a>
        </Scroll>
      </footer>
    </section>

    <article id="first" className="container box style1 right">
      <div className="image fit">
        <img src={pic1} alt="" />
      </div>
      <div className="inner">
        <header>
          <h2>
            Ihre Webseite
          
          
          </h2>
        </header>
        <p>
         Wir liefern richtige template Webseiten 
          <br></br>
        je nach grösse gratis oder mit Aufpreis
        <br></br>

        </p>
      </div>
    </article>

    <article className="container box style1 left">
      <a href="/#" className="image fit">
        <img src={pic2} alt="" />
      </a>
      <div className="inner">
        <header>
          <h2>
Nachhaltige Investition            <br />
            
          </h2>
        </header>
        <p>
          Wir bauen mit einem der neuen Standarts und bieten Ihnen dadurch eine Webseite für die kommenden Jahre.
        </p>
      </div>
    </article>

    <section id="banner2">
      <header>
        <h2>Unsere Services</h2>
        <h3>Wir bieten das volle Programm</h3>
      </header>
      <p>
      Es gibt zu viele Firmen die eine alte Webseite haben und noch viel mehr,<br></br> welche nicht von ihrer Webseite profitieren.

          <br></br> 
        
          <br></br>

          - Wir optimieren und polieren Ihren Webauftritt - 


        
          

        {/* A brand new site template designed by{' '}
        <a href="http://twitter.com/ajlkn">AJ</a> for{' '}
        <a href="http://html5up.net">HTML5 UP</a>.<br />
* 
        Wir designen wirklich gerne Webseiten.
        <br />
        {' '}
      
        <a href="http://html5up.net/license">Creative Commons license</a>.
         */}

      </p>
      <footer>
        <Scroll type="id" element="second">
          <a href="#second" className="button style2 scrolly">
           Mehr dazu
          </a>
        </Scroll>
      </footer>
    </section>

    <article id="second" className="container box style1 right">
      <a href="/#second" className="image fit">
        <img src={pic3} alt="" />
      </a>
      <div className="inner">
        <header>
          <h2>
                StartUp          <br />
            
          </h2>
        </header>
        <p>
          Wir sind ein StartUp aus Zürich. Spezialisiert für Web, Mobile & Digital Marketing
        </p>
      </div>
    </article>

    <article className="container box style1 left">
      <a href="/#" className="image fit">
        <img src={pic4} alt="" />
      </a>
      <div className="inner">
        <header>
          <h2>
              Technik         <br />
            
          </h2>
        </header>
        <p>

     
          Wir bauen mit    <a href="http://GatsbyJS.com">GatsbyJS</a>. <br></br> 
          <br></br> Verlässlich schnelle Webseiten mit gutem SEO Potential<br></br>
          JavaScript, HTML, CSS
        </p>
      </div>
    </article>

    <article className="container box style2">
    <a href="https://block-lab.ch"> <header>
        <h2> Block Lab GmbH</h2>
        <p>
         Web Mechaniker & Digital Laborant
          <br />
          Besuchen Sie unsere Webseite
        </p>
      </header>
      </a>

      {/* <div className="inner gallery">
        <Gallery
          images={ROW1_IMAGES.map(({ src, thumbnail, caption }) => ({
            src,
            thumbnail,
            caption,
          }))}
        />
        <Gallery
          images={ROW2_IMAGES.map(({ src, thumbnail, caption }) => ({
            src,
            thumbnail,
            caption,
          }))}
        />
      </div> */}
    </article>

    <article className="container box style3">
      <header>
        <h2>Kontaktieren Sie uns</h2>
        <p>Formular Ausfüllen gleich hier oder schreib uns per WhatsApp</p>
      </header>
      {/* action="#"

      <form name="contact" method="post" data-netlify="true" >
  You still need to add the hidden input with the form name to your JSX form 
 
  ...
</form>
*/}
      
      <form name="corona-hilf" method="post" data-netlify="true" onSubmit="submit" data-netlify-honeypot="bot-field">
      <input type="hidden" name="formula1" value="corona-hilf" />
        <div className="row gtr-50">
          <div className="col-6 col-12-mobile">
            <input
              type="text"
              className="text"
              name="name"
              placeholder="Name"
            />
          </div>
          <div className="col-6 col-12-mobile">
            <input
              type="text"
              className="text"
              name="email"
              placeholder="Email"
            />
          </div>
              {/* <div className="checkbox-wrap">
            <input 
            type="checkbox"
            value="check"
            classname="checkbox"
            name="checky"
            /> Check 1
                               <span class="checkmark"></span>

          </div>     */}
          <div className="col-12">
            <textarea name="message" placeholder="Nachricht - Erklären Sie uns was Sie brauchen" />
          </div>

          {/* <div className="col-6" class="field">
          <input type="file" 
          name="filename" 
          id="myfile"
          placeholder="Datei Anhängen"
          />
          </div> */}

          <div className="col-12">
            <ul className="actions">
              <li>
                <input type="submit" value="Senden" />
              </li>
            </ul>
          </div>


        </div>
      </form>
    </article>

    <article className="container box style3">
      <header>
        <h2>Templates</h2>
        <p>Sie können die herkömmlichen Templates auch selber nach einer Webseite durchsuchen</p>
        <br />
        Mehr dazu bei 
        <Link to="https://www.gatsbyjs.com/starters/"> GatsbyJS </Link>
      </header>
    </article>

    <Footer />
  </Layout>
);

export default IndexPage;
